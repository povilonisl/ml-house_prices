import pandas as pd
import numpy as np
import random
import sklearn.linear_model as lm
from sklearn.cluster import KMeans
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.feature_selection import VarianceThreshold
from multiprocessing.dummy import Pool as ThreadPool 

SEED = 30
# np.random.seed(SEED)
# random.seed(SEED)


cities = [{'name_ocean': 'NEAR LA', 'latitude': 34.0522, 'longitude': -118.2437, 'latitudeArea': 0.11 , 'longitudeArea': 0.26 },
        {'name_ocean': 'NEAR SD', 'latitude': 32.7157, 'longitude': -117.1611, 'latitudeArea': 0.22 , 'longitudeArea': 0.28 },
        {'name_ocean': 'NEAR SJ', 'latitude': 37.3382, 'longitude': -121.8863, 'latitudeArea': 0.18 , 'longitudeArea': 0.37 },
        {'name_ocean': 'NEAR SF', 'latitude': 37.7749, 'longitude': -122.4194, 'latitudeArea': 0.12 , 'longitudeArea': 0.11 },
        {'name_ocean': 'NEAR FR', 'latitude': 36.7468422, 'longitude': -119.7725868, 'latitudeArea': 0.2 , 'longitudeArea': 0.15 }]


def changeOceanProximity(housing, index):
    for city in cities:
        if ((abs(housing.latitude.values[index] - city['latitude']) <= city['latitudeArea']) and
            (abs(housing.longitude.values[index] - city['longitude']) <= city['longitudeArea'])):
            housing.ocean_proximity.values[index] = city['name_ocean']


def changeOceanProximityFromData(housing):

    for index, row in housing.iterrows():
        changeOceanProximity(housing, index)
    return housing



def one_hot_encode(housing,test_housing):
    return pd.get_dummies(housing),pd.get_dummies(test_housing)

def fill_na_with_model(model,housing):
    isna = housing.total_bedrooms.isna()
    missing_bedrooms = model.predict(housing.total_rooms.values[isna].reshape(-1, 1))
    housing.loc[isna,"total_bedrooms"] = np.squeeze(missing_bedrooms)
    return housing

def impute_total_bedrooms_with_lin_reg(housing, test_housing):
    notna = housing.total_bedrooms.notna()
    model = lm.LinearRegression()
    model.fit(housing.households.values[notna].reshape(-1, 1), housing.total_bedrooms.values[notna].reshape(-1, 1))
    return fill_na_with_model(model,housing),fill_na_with_model(model,test_housing)

def drop_missing_total_bedrooms(housing,test_housing):
    return housing.dropna(),test_housing.dropna()

def missing_total_bedrooms_fill_mean(housing,test_housing):
    mean = housing.total_bedrooms.mean()
    housing.loc[housing.total_bedrooms.isna(),"total_bedrooms"]  = mean
    test_housing.loc[test_housing.total_bedrooms.isna(),"total_bedrooms"]  = mean
    return housing,test_housing

def missing_total_bedrooms_fill_median(housing,test_housing):
    mean = housing.total_bedrooms.median()
    housing.loc[housing.total_bedrooms.isna(),"total_bedrooms"]  = mean
    test_housing.loc[test_housing.total_bedrooms.isna(),"total_bedrooms"]  = mean
    return housing,test_housing

def noOp(housing,test_housing):
    return housing,test_housing

def train_test_split_df(df,test_size=0.1):
    test_idxs = random.sample(range(len(df)), int(len(df)*test_size))
    return df.drop(test_idxs),df.loc[test_idxs]

def housing_to_x_y(housing):
    x = housing.drop(columns=['median_house_value'])
    y = housing.median_house_value
    return x,y

def remove_outliers(housing,test_housing):
    for col in ['population','total_bedrooms','households','total_rooms']:
        q = housing[col].quantile(0.95)
        housing = housing[housing[col] < q]
        test_housing = test_housing[test_housing[col] < q]
    return housing,test_housing

def lat_long_grid(housing, test_housing):
    def bin(col):
        long = np.linspace(housing[col].min(),housing[col].max(), 20)
        return np.digitize(housing[col],long), np.digitize(test_housing[col],long)

    long_bins,long_bins_test = bin('longitude')
    lat_bins,lat_bins_test = bin('latitude')

    housing['grid'] = [int(str(a)+str(b)) for a,b in zip(long_bins,lat_bins)]
    housing['grid'] = housing['grid'].astype('category')

    test_housing['grid'] = [int(str(a)+str(b)) for a,b in zip(long_bins_test,lat_bins_test)]
    test_housing['grid'] = pd.Categorical(test_housing['grid'], categories=housing['grid'].cat.categories)

    housing = pd.get_dummies(housing,columns=['grid'])
    test_housing = pd.get_dummies(test_housing,columns=['grid'])
    return housing,test_housing

def cluster(housing,test_housing):
    kmeans = KMeans(n_clusters=50, random_state=0,max_iter=70).fit(list(zip(housing.longitude,housing.latitude)))

    housing['cluster'] = kmeans.predict(list(zip(housing.longitude,housing.latitude)))
    housing['cluster'] = housing['cluster'].astype('category')

    test_housing['cluster'] = kmeans.predict(list(zip(test_housing.longitude,test_housing.latitude)))
    test_housing['cluster'] = pd.Categorical(test_housing['cluster'], categories=housing['cluster'].cat.categories)

    test_housing = pd.get_dummies(test_housing,columns=['cluster'])
    housing = pd.get_dummies(housing,columns=['cluster'])
    return housing,test_housing



def min_max_norm(housing,test_housing):
    min_max = MinMaxScaler()
    housing[housing.columns] = min_max.fit_transform(housing[housing.columns])
    test_housing[test_housing.columns] = min_max.transform(test_housing[test_housing.columns])
    return housing,test_housing


def standard_scaler(housing, test_housing):
    std = StandardScaler()
    housing[housing.columns] = std.fit_transform(housing[housing.columns])
    test_housing[test_housing.columns] = std.transform(test_housing[test_housing.columns])
    return housing, test_housing
    
class Runner():

    def __init__(self,housing_file="housing.csv"):
        housing = pd.read_csv('housing.csv')
        housing = changeOceanProximityFromData(housing)
        housing['ocean_proximity'] = housing['ocean_proximity'].astype('category')

        self.housing, self.holdout_housing = train_test_split_df(housing)


        #self.poly = PolynomialFeatures(degree=2)
        #self.clf = Ridge(alpha=0.05, normalize=False)
        #self.lasso_reg = Lasso(alpha=0.7, normalize=True, max_iter=1e3)
        #self.sel = VarianceThreshold(threshold=0.52)

        
        self.results = []

    def run(self,total_bedroom_imputer,ocean_proximity_handler,normaliser=noOp,regulariser=noOp):
        poly = PolynomialFeatures(degree=2)
        clf = Ridge(alpha=1.2, normalize=False, max_iter=1e2)
        lasso_reg = Lasso(alpha=0.7, normalize=True, max_iter=1e3)
        #sel = VarianceThreshold(threshold=0.52)
        sel = VarianceThreshold(threshold=0.008)
        
        fold_results = []
        fold_train_results = []

        clf_train_results = []
        clf_results = []
        
        # + [remove_outliers,lat_long_grid]
        modifiers = [total_bedroom_imputer,ocean_proximity_handler,normaliser,regulariser]
        
        kf = KFold(n_splits=5, shuffle=True, random_state=SEED)
        for train_index, test_index in kf.split(self.housing):
            
            train_housing = self.housing.iloc[train_index]
            test_housing = self.housing.iloc[test_index]
        
            for modifier in modifiers + [remove_outliers, lat_long_grid,cluster]:
                train_housing,test_housing = modifier(train_housing.copy(),test_housing.copy())
            
            x_train,y_train = housing_to_x_y(train_housing)
            x_test,y_test = housing_to_x_y(test_housing)
        
            poly_x_train = poly.fit_transform(x_train)
            poly_x_test = poly.transform(x_test)
            
            poly_x_train = sel.fit_transform(poly_x_train)
            poly_x_test = sel.transform(poly_x_test)
            
            clf.fit(poly_x_train, y_train)
            clf_train_results.append(clf.score(poly_x_train, y_train))
            clf_results.append(clf.score(poly_x_test, y_test))

            #lasso_reg.fit(poly_x_train, y_train)
            #clf_train_results.append(lasso_reg.score(poly_x_train, y_train))
            #clf_results.append(lasso_reg.score(poly_x_test, y_test))
            

        print("All train results: ", np.mean(clf_train_results))
        print("All test results: ", np.mean(clf_results))

    
    def save(self,file_name):
        df = pd.DataFrame(self.results,columns=["total_bedroom_imputer","ocean_proximity_handler","normaliser","regulariser","result"])
        df.to_csv(file_name,index=False)

if __name__ == '__main__':
    runner = Runner()

    runner.run(impute_total_bedrooms_with_lin_reg, one_hot_encode, normaliser = standard_scaler)

    runner.save("results.csv")



"""
Grid 10
No feature selection
lasso
0.736



Grid 20
Feature selection
lasso
0.7557


"""
