# ML-house_prices

We were to predict house prices in California given various summarised attributes of the houses. I have included the source code we have used to predict the house prices, as well as the report we have submitted on our findings.





Contributors:

*  Filippo Galli - https://www.linkedin.com/in/gallifilo/

*  Clemens Lang

*  George Oversby


*  Lukas Povilonis - https://www.linkedin.com/in/lukas-povilonis-64a900a1/